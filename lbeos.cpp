#include "lbeos.h"

LBfloat eos_ideal(LBfloat rho,LBfloat T)
{
	return rho/a2;
}

LBfloat eos_waals(LBfloat rho,LBfloat T)
{
	return (8. * rho * T/(3.-rho) - 3. * rho * rho);
}