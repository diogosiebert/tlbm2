#include "lbifstream.h"

LBifstream::LBifstream()
{

}

int LBifstream::read_int(string field)
{
	*this >> text;
	if ( text.find_first_not_of("-0123456789") != string::npos ) 
	{
		cout << "(" << this->get_line() << ") " << "Error reading " << field << " field!" << endl;
		exit(3);
	}
	return  atoi(text.c_str());
}

bool LBifstream::compare_next(string tobecompared)
{
	if (this->eof()) return 0;
	else
	{
		*this >> text;
		return !text.compare(tobecompared);
	}
}

float LBifstream::read_float(string field)
{
	*this >> text;
	if ( text.find_first_not_of("-.0123456789eE") != string::npos ) 
	{
		cout << "(" << this->get_line() << ") " << "Error reading " << field << " field!" << endl;
		exit(3);
	}
	return  atof(text.c_str());
}

bool LBifstream::seek_string(string tobefound)
{
	string text;
	this -> clear();
	this -> seekg(0,ios::beg);
	do
	{
		if (this -> eof()) return 0;
		*this >> text;
	} 
	while (text.compare(tobefound));
	return 1;
}

bool LBifstream::seek_string(string tobefound,string beg,string end)
{
	string text;
	streampos beg_pos,end_pos;

	if (!this -> seek_string(beg))  return 0;
	beg_pos = this -> tellg();

	if (!this -> seek_string(end)) return 0;
	end_pos = this -> tellg();	

	if (end_pos - beg_pos <= 0) return 0;

	this -> seekg(beg_pos);

	do
	{
		if (!text.compare(end)) return 0;
		*this >> text;
	} 
	while (text.compare(tobefound));
	return 1;
}

int LBifstream::get_line()
{
	int line = 1;
	streampos pos = this -> tellg();
	this -> clear();
	this -> seekg(0,ios::beg);
	do
	{
		if (this->get() == '\n') line++;
	} while (this->tellg() != pos);
	return line;
}

int LBifstream::read_field(string name,int type,int n,void* p)
{
	if(seek_string(name))
	{
		if (type == INT)
		{
			int* pointer = (int*) p;
			for (int i=0;i<n;i++)
			{	
				pointer[i] = read_int(name);
			}
		}
		else if (type == FLOAT)
		{
			LBfloat* pointer = (LBfloat*) p;
			for (int i=0;i<n;i++)
			{	
				pointer[i] = read_float(name);
			}
		}
		return 1;
	}
	else 
	{
		return 0;
	}
}

void error(string comment)
{
	cout << comment << endl;
	exit(3);
}
