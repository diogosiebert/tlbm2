#ifndef LBEOS_H
#define LBEOS_H

//#define rhoc 4.
//#define pc 0.0025

#include "lbdata.h"

LBfloat eos_ideal(LBfloat rho,LBfloat T);
LBfloat eos_waals(LBfloat rho,LBfloat T);

#endif
