#ifndef D2V17_H
#define D2V17_H

#include <string>
#include "../lbmath.h"

using namespace std;

namespace d2v17
{
	const string lattice_name = "D2V17";
	const int b = 17;
	const int bin = 8;

	typedef LBfloat LBdistribution[b];

	const LBfloat a2 = 2.700864165934014201974482433404760724258E0;
	const LBfloat a4 = 7.294667242826438201300286227099863625896E0;
	const LBfloat a6 = 1.970190535856260305595693321894794163660E1;
	const LBfloat a8 = 5.321217018356506991380819959266674812870E1;
	const int cx[b]= {0, -1, 0, 0, 1, -1, -1, 1, 1, -2, -2, 2, 2, -3, 0, 0, 3};
	const int cy[b]= {0, 0, -1, 1, 0, -1, 1, -1, 1, -2, 2, -2, 2, 0, -3, 3, 0};
	const int cz[b]= {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

	const LBfloat w[b]  = {4.020051469091126259416643924590754326986E-1, 
							1.161548664977815438740354566259111970518E-1, 
							1.161548664977815438740354566259111970518E-1,  
							1.161548664977815438740354566259111970518E-1,  
   							1.161548664977815438740354566259111970518E-1,  
   							3.300635362298691394975382591472168241846E-2,  
   							3.300635362298691394975382591472168241846E-2,  
   							3.300635362298691394975382591472168241846E-2,  
   							3.300635362298691394975382591472168241846E-2,  
   							7.907860216591813123713324054816357339718E-5,  
   							7.907860216591813123713324054816357339718E-5,  
   							7.907860216591813123713324054816357339718E-5,  
   							7.907860216591813123713324054816357339718E-5,  
   							2.584145497874675595574861040500987817219E-4,  
   							2.584145497874675595574861040500987817219E-4,  
   							2.584145497874675595574861040500987817219E-4,  
   							2.584145497874675595574861040500987817219E-4};
							
	const LBfloat cc[b]  = {0, 1, 1, 1, 1, 2, 2, 2, 2, 8, 8, 8, 8, 9, 9, 9, 9};

	const int in[bin] = {1, 2, 5, 6, 9, 10, 13, 14};
	const int out[bin] = {4, 3, 8, 7, 12, 11, 16, 15};

	class LBmacro
	{
		public:
		LBmacro();
		LBmacro(LBdistribution f);
		LBmacro(LBfloat r,LBfloat x ,LBfloat y,LBfloat z);
		LBmacro(LBfloat r,LBfloat x ,LBfloat y,LBfloat z,LBfloat tmp);
		void set(LBfloat r,LBfloat x,LBfloat y,LBfloat z);
		void set(LBfloat r,LBfloat x,LBfloat y,LBfloat z,LBfloat tmp);
		LBfloat rho;
		LBvector u;
		LBfloat tmp;
		void calculate(LBdistribution f);
		void equilibrium(LBdistribution &f);
	};
	
	LBfloat density(LBdistribution f);
}

#endif
