#include "d2v25w6.h"

using namespace d2v25w6;

LBmacro::LBmacro()
{
	set(1.,0.,0.,0.,0.);
}

LBmacro::LBmacro(LBfloat r ,LBfloat x ,LBfloat y,LBfloat z,LBfloat tmp)
{
	set(r,x,y,z,tmp);
}

LBmacro::LBmacro(LBfloat r ,LBfloat x ,LBfloat y,LBfloat z)
{
	set(r,x,y,z,0.0);
}

LBmacro::LBmacro(LBdistribution f)
{
	calculate(f);
}

void LBmacro::set(LBfloat r, LBfloat x, LBfloat y, LBfloat z)
{
	set(r,x,y,z,0.0);
}

void LBmacro::set(LBfloat r, LBfloat x, LBfloat y, LBfloat z,LBfloat _tmp)
{
	rho = r;
	u.set(x,y,z);
	tmp = _tmp;
}

void LBmacro::calculate(LBdistribution f)
{
	LBfloat ux = 0;
	LBfloat uy = 0;
	LBfloat e = 0;
	rho = f[0];
		
	for (int i=1;i<b;i++) 
	{
	    rho += f[i];
	    ux  += f[i] * cx[i];
	    uy  += f[i] * cy[i];
	    e   += f[i] * cc[i];
	}
	
	if (rho != 0 )
	{
		ux /= rho;
		uy /= rho;
		tmp = a2*0.5*(e/rho - ux*ux - uy*uy)-1;
	}
	else
	{
		ux = 0;
		uy = 0;
		tmp = 0;
	}

	u(0) = ux;
	u(1) = uy;
}

void LBmacro::calculate(LBdistribution f,LBvector g)
{
	calculate(f);
	u = u + 0.5 * g;
}

void LBmacro::equilibrium(LBdistribution &f)
{
/*	long double uc,uu = u*u;
	tmp = 0;
	for (int i=0;i<b;i++)
	{
		uc = (cx[i]*u(0) + cy[i]*u(1));
		f[i] = rho* w[i] * (
		+1.00000000000000000000000000000
		-0.578464834591373208759336783236 * uu
		+1.15692966918274641751867356647  * uc
		+0.669243129717649532847512937136 * uc*uc
		-1.00000000000000000000000000000  * tmp
		+0.578464834591373208759336783236 * tmp * cc[i]
		-2.31385933836549283503734713295  * tmp * uc
		+0.669243129717649532847512937136 * tmp * cc[i] * uc
		+0.258089077555688707300054388184 * uc * uc * uc
		-0.669243129717649532847512937136 * uu * uc
		+1.00000000000000000000000000000  * tmp * tmp
		-1.15692966918274641751867356647  * tmp * tmp * cc[i]
		+0.167310782429412383211878234284 * tmp * tmp * cc[i] * cc[i]
		+1.15692966918274641751867356647  * tmp * uu
		-0.334621564858824766423756468568 * tmp * uu * cc[i]
//		+0								  * tmp * uu * cc[i] * cc[i]
		-2.00772938915294859854253881141  * tmp * uc * uc
		+0.387133616333533060950081582276 * tmp * uc * uc  * cc[i]
//		+0								  * tmp * uc * uc  * cc[i] * cc[i]
		+0.167310782429412383211878234284 * uu * uu
//		+0								  * uu * uu * cc[i]
		-9.3309659723807224852403443298E-2* uu * uu * cc[i] * cc[i]
		-0.387133616333533060950081582276 * uc * uc * uu 
		+0.0746477277790457798819227546384* uc * uc * uu * cc[i] 
		);		
	} */
	
	LBfloat cc2,uc,uu = a2*(u(0)*u(0) + u(1)*u(1));
	LBfloat tt  = 0;

	for (int i=0;i<b;i++)
	{
		cc2 = a2 * (cx[i]*cx[i] + cy[i]*cy[i]);
		uc =  a2 * (cx[i]*u(0) + cy[i]*u(1));
		f[i] = rho * w[i] * (1.0 + uc + 0.5*uc*uc - 0.5*uu + tt*(0.5*cc2-1.0) + uc*uc*uc/6.0 + 0.5*tt*cc2*uc 
				- 0.5*uu*uc - 2.0*tt*uc + tt*tt - tt*tt*cc2 + tt*tt*cc2*cc2/8.0 + tt*uu - tt*uu*cc2/4.0 
				- 3.0*tt*uc*uc/2.0 + tt*uc*uc*cc2/4.0 + uu*uu/8.0 - uc*uc*uu/4.0 - cc2*cc2*uu*uu/192.0 + cc2*uu*uc*uc/24.0);
	}
  
}

