#include "d2q9.h"

using namespace d2q9;

LBmacro::LBmacro()
{
	set(1,0,0,0);
}

LBmacro::LBmacro(LBfloat r ,LBfloat x ,LBfloat y,LBfloat z)
{
	set(r,x,y,z);
}

LBmacro::LBmacro(LBdistribution f)
{
	calculate(f);
}

void LBmacro::set(LBfloat r, LBfloat x, LBfloat y, LBfloat z)
{
	rho = r;
	u.set(x,y,z);
}

void LBmacro::calculate(LBdistribution f)
{
	LBfloat ux = 0;
	LBfloat uy = 0;
	// LBfloat e = 0; (Não necessario em modelo isotérmico)
	rho = f[0];
		
	for (int i=1;i<b;i++) 
	{
	    rho += f[i];
	    ux  += f[i] * cx[i];
	    uy  += f[i] * cy[i];
	    
	    /* Modelo Isotérmico (Não é preciso calcular a energia interna)
	    e += f[i] * (cx[i]*cx[i]+cy[i]*cy[i]);
	    */
    
	}
	
	if (rho != 0 )
	{
		ux /= rho;
		uy /= rho;
	//	tmp = a2*0.5*(e - ux*ux - uy*uy)/rho-1.0;
	//	Imposição de Modelo Isotérmico
		tmp = 0;
	}
	else
	{
		ux = 0;
		uy = 0;
		tmp = 0;
	}

	u(0) = ux;
	u(1) = uy;
}

void LBmacro::equilibrium(LBdistribution &f)
{
	LBfloat uc,uu = u*u;
	tmp = 0;
	
	for (int i=0;i<b;i++)
	{
		uc = (cx[i]*u(0) + cy[i]*u(1));
		f[i] = rho*w[i] * (
					1.0 + a2*uc + 0.5*a4*uc*uc - 0.5*a2*uu + tmp*(0.5*a2*cc[i]-1) 
				);
		
	}

}  

LBfloat d2q9::density(LBdistribution f)
{
	LBfloat r = 0;
	for (int i=0;i<b;i++) r += f[i];
	return r;
}

