#ifndef D2Q9_H
#define D2Q9_H

#include "../lbmath.h"
#include <string>

using namespace std;

namespace d2q9
{
	const string lattice_name = "D2Q9";
	
	const int b = 9;
	const int bin = 4;
	
	typedef LBfloat LBdistribution[b];
	
	const LBfloat a2 = 3.000000000000000000000000000000000000000E0;
	const LBfloat a4 = 9.000000000000000000000000000000000000000E0;
	const LBfloat a6 = 2.700000000000000000000000000000000000000E1;
	
	const int cx[b]= {0,-1, 0, 0, 1, -1, -1, 1, 1};
	const int cy[b]= {0, 0,-1, 1, 0, -1,  1,-1, 1};
	const int cz[b]= {0, 0, 0, 0, 0,  0,  0, 0, 0};
	
	const int in[bin] =  {1, 2, 5, 6};
	const int out[bin] = {4, 3, 8, 7}; 

	const LBfloat cc[b]  = {0, 1, 1, 1, 1, 2, 2, 2, 2};
	
	const LBfloat w[b]  = {	4.444444444444444444444444444444444444444E-1, 
							1.111111111111111111111111111111111111111E-1, 
 							1.111111111111111111111111111111111111111E-1, 
							1.111111111111111111111111111111111111111E-1, 
 							1.111111111111111111111111111111111111111E-1, 
 							2.777777777777777777777777777777777777778E-2, 
							2.777777777777777777777777777777777777778E-2, 
							2.777777777777777777777777777777777777778E-2, 
							2.777777777777777777777777777777777777778E-2};
	
	class LBmacro
	{
		public:
		LBmacro();
		LBmacro(LBfloat r,LBfloat x ,LBfloat y,LBfloat z);
		LBmacro(LBdistribution f);
		void set(LBfloat r,LBfloat x,LBfloat y,LBfloat z);
		LBfloat rho;
		LBvector u;
		LBfloat tmp;
		void calculate(LBdistribution f);
		void equilibrium(LBdistribution &f);
	}; 
	
	LBfloat density(LBdistribution f);	
}



#endif
