#ifndef D2V25W6_H
#define D2V25W6_H

#include "../lbmath.h"

namespace d2v25w6
{
	const int b = 25;   // Numero de velocidades
	const int bin = 12;  // Número de pares que formaram os swaps
	
	const LBfloat a2 = 1.1569296691827464175186735664726338352224669427522;

	const LBfloat w0 = 0.23905788376748833308837791962485897992531583149355;
	const LBfloat w1 = 6.3158995428181604652625110011856689752157009033676E-2;
	const LBfloat w2 = 8.7594578033267925750187248444574113646403114934383E-2;
	const LBfloat w3 = 3.1179030982623790958357796198314338914970198419962E-2;
	const LBfloat w4 = 6.1989738391789159848927659404912413657035361580584E-3;
	const LBfloat w5 = 2.0201271524446506040238246105352072759826308943803E-3;
	const LBfloat w6 = 8.3823622431028777818774888013664063454552686153939E-5;
	
	typedef LBfloat LBdistribution[b];
	
	/* Definição dos vetores de rede */
	
			// 0   1   2   3   4  5   6   7    8  9  10  11 12 13 14 15 16
	
	const int cx[b] = {0 , 1 , 0, -1,  0, 1,  -1, -1,  1, 2 , 0, -2,  0 , 2, -2, -2, 2, 3, 0,-3, 0, 4 , 0, -4,  0};
	const int cy[b] = {0 , 0  ,1,  0, -1, 1,   1, -1, -1, 0  ,2,  0, -2,  2,  2, -2,-2, 0, 3, 0,-3, 0  ,4,  0, -4};
	const int cz[b] = {0 , 0  ,0,  0,  0, 0,   0,  0,  0, 0,  0,  0,  0,  0,  0,  0, 0, 0 ,0 ,0, 0, 0,  0  ,0 , 0};

	const int in[bin] =  {1,2,5,6, 9,10,13,14,17,18,21,22};
	const int out[bin] = {3,4,7,8,11,12,15,16,19,20,23,24};

	const int dx[9] = {0 , 1 , 0 ,-1,   0, 1,  -1, -1, 1};
	const int dy[9] = {0 , 0  ,1,   0, -1, 1,   1, -1,-1};
	const int dz[9] = {0 , 0  ,0,   0,  0, 0,   0,  0, 0};

	const int level[b] = {0, 1,  1 , 1 , 1 , 1 , 1 , 1 , 1, 2 , 2 ,2 ,2, 2 , 2 , 2 , 2, 3 ,3 , 3, 3, 4 , 4 , 4 , 4};
	const int d[b]     = {0, 1,  2 , 3 , 4 , 5 , 6 , 7,  8, 1,  2 ,3 ,4 ,5 , 6 , 7,  8, 1, 2 , 3 ,4 , 1, 2 , 3 , 4};

	const int reflect[9] = {0,3,4,1,2,7,8,5,6};
	
	const int Mirrorx[b] = {0,3,2,1,4,6,5,8,7};
	const int Mirrory[b] = {0,1,4,3,2,8,7,6,5};
	const int Mirrorz[b] = {0,1,2,3,4,5,6,7,8};
	
	const LBfloat w[b]  = {w0,w1,w1,w1,w1,w2,w2,w2,w2,w3,w3,w3,w3,w4,w4,w4,w4, w5, w5 , w5, w5, w6,w6,w6,w6};
	
	const LBfloat cc[b] = {0,1,1,1,1,2,2,2,2,4,4,4,4,8,8,8,8,9,9,9,9,16,16,16,16};
	
	class LBmacro
	{
		public:
		LBmacro();
		LBmacro(LBfloat r,LBfloat x ,LBfloat y,LBfloat z);
		LBmacro(LBfloat r,LBfloat x ,LBfloat y,LBfloat z,LBfloat tmp);
		LBmacro(LBdistribution f);
		void set(LBfloat r,LBfloat x,LBfloat y,LBfloat z);
		void set(LBfloat r,LBfloat x,LBfloat y,LBfloat z,LBfloat tmp);
		LBfloat rho;
		LBvector u;
		LBfloat tmp;
		void calculate(LBdistribution f);
		void calculate(LBdistribution f,LBvector g);
		void equilibrium(LBdistribution &f);
	};
	
}

#endif
