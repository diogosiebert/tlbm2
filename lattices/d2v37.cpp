#include "d2v37.h"

using namespace d2v37;

LBmacro::LBmacro()
{
	set(1.,0.,0.,0.,0.);
}

LBmacro::LBmacro(LBfloat r ,LBfloat x ,LBfloat y,LBfloat z)
{
        set(r,x,y,z);
}

LBmacro::LBmacro(LBfloat r ,LBfloat x ,LBfloat y,LBfloat z,LBfloat tmp)
{
	set(r,x,y,z,tmp);
}

LBmacro::LBmacro(LBdistribution f)
{
	calculate(f);
}

void LBmacro::set(LBfloat r, LBfloat x, LBfloat y, LBfloat z)
{
	set(r,x,y,z,0);
}

void LBmacro::set(LBfloat r, LBfloat x, LBfloat y, LBfloat z,LBfloat _tmp)
{
	rho = r;
	u.set(x,y,z);
	tmp = _tmp;
}

void LBmacro::calculate(LBdistribution f)
{
	LBfloat ux = 0;
	LBfloat uy = 0;
	// LBfloat e = 0; (Não necessario em modelo isotérmico)
	rho = f[0];
		
	for (int i=1;i<b;i++) 
	{
	    rho += f[i];
	    ux  += f[i] * cx[i];
	    uy  += f[i] * cy[i];
	    
	    /* Modelo Isotérmico (Não é preciso calcular a energia interna)
	    e += f[i] * (cx[i]*cx[i]+cy[i]*cy[i]);
	    */
    
	}
	
	if (rho != 0 )
	{
		ux /= rho;
		uy /= rho;
	//	tmp = a2*0.5*(e - ux*ux - uy*uy)/rho-1.0;
	//	Imposição de Modelo Isotérmico
		tmp = 0;
	}
	else
	{
		ux = 0;
		uy = 0;
		tmp = 0;
	}

	u(0) = ux;
	u(1) = uy;
}

void LBmacro::equilibrium(LBdistribution &f)
{
	LBfloat uc,uu = u*u;
	tmp = 0;
	
	for (int i=0;i<b;i++)
	{
		uc = (cx[i]*u(0) + cy[i]*u(1));
		f[i] = rho*w[i] * (
					1.0 + a2*uc + 0.5*a4*uc*uc - 0.5*a2*uu + tmp*(0.5*a2*cc[i]-1) 
					+ 0.5*a4*tmp*cc[i]*uc  +  a6/6. *uc*uc* uc  - 2.*a2*tmp*uc  - 0.5*a4*uu*uc
					+ tmp*tmp - a2*cc[i]*tmp*tmp + 0.125*a4*cc[i]*cc[i]*tmp*tmp - 1.5*a4*tmp*uc*uc + 0.25*a6*cc[i]*tmp*uc*uc + a8*uc*uc*uc*uc/24. + a2*tmp*uu - 0.25*a4*cc[i]*tmp*uu - 0.25*a6*uc*uc*uu + 0.125*a4*uu*uu
				  );
		
	}
}

LBfloat d2v37::density(LBdistribution f)
{
	LBfloat r = 0;
	for (int i=0;i<b;i++) r += f[i];
	return r;
}
