#ifndef STREAM_H
#define STREAM_H

#include "lbdata.h"



void stream(LBdistribution* f);
inline void floatswitch(LBfloat &a,LBfloat &b);
void record_neighbors(LBdata& data);
	
#endif