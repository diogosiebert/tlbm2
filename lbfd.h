#ifndef LBFD_H
#define LBFH_H

#include "lbmath.h"

// Gradiente esquema 1 - Erro de Ordem O(h^4)

namespace gradient_s8
{
	const int g_np = 8;
	
	int g_ex[g_np] = { 1, 0,-1, 0, 2, 0,-2, 0};
	int g_ey[g_np] = { 0, 1, 0,-1, 0, 2, 0,-2};
	int g_ez[g_np] = { 0, 0, 0, 0, 0, 0, 0, 0};
	
	LBfloat g_w[g_np] = {2./3.,2./3.,2./3.,2./3.,-1./24.,-1./24.,-1./24.,-1./24.};
}

// Gradiente Esquema com 16 pontos

namespace gradient_s16
{
	const int g_np = 16;
	
	int g_ex[g_np] = { 1, 0,-1, 0, 1,-1,-1, 1, 2, 0,-2, 0, 2,-2,-2, 2};
	int g_ey[g_np] = { 0, 1, 0,-1, 1, 1,-1,-1, 0, 2, 0,-2, 2, 2,-2,-2};
	int g_ez[g_np] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	LBfloat g_w[g_np] = { 4./3.,4./3.,4./3.,4./3., -1./3., -1./3., -1./3., -1./3.,
						-1./12. , -1./12., -1./12., -1./12., 1./48., 1./48., 1./48.,  1./48.};
}

// Laplaciano esquema com 9 pontos

namespace laplacian_s9
{
	const int l_np = 9;
	
	int l_ex[l_np] = { 0, 1, 0,-1, 0, 2, 0,-2, 0};
	int l_ey[l_np] = { 0, 0, 1, 0,-1, 0, 2, 0,-2};
	int l_ez[l_np] = { 0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	LBfloat l_w[l_np] = {-5., 4./3., 4./3., 4./3., 4./3.,
						-1./12., -1./12., -1./12., -1./12. };
	
}

// Laplaciano esquema com 21 pontos

namespace laplacian_s21
{
	const int l_np = 21;
	
	int l_ex[l_np] = { 1, 0,-1, 0, 1,-1,-1, 1, 2,-2,-2, 2, 2, 1,-2, 1, 2,-1,-2,-1};
	int l_ey[l_np] = { 0, 1, 0,-1, 1, 1,-1,-1, 2, 2,-2,-2, 1, 2, 1,-2,-1, 2,-1,-2};
	int l_ez[l_np] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	LBfloat l_w[l_np] = {-9./2., 1., 1., 1., 1., 2./9., 2./9., 2./9., 2./9.,
						  1./72., 1./72., 1./72. , 1./72., -1./18., -1./18., -1./18., -1./18. };
}

#endif