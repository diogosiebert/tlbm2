#ifndef READSIM_H
#define READSIM_H

#include <iostream>
#include "lbifstream.h"
#include "lattices/d2q9.h"
#include "lattices/d2v17.h"
#include "lattices/d2v25w6.h"
#include "lattices/d2v37.h"
#include "lattices/d2v33.h"
#include <vector>

using namespace d2v37;
using namespace std;

class LBdata
{
	public:
		LBdata(char* filename);
		int Dim;
		int Dx, Dy, Dz;
		LBfloat tau;
		LBfloat rc;
		LBfloat pc;
		LBvector F;
		int steps;
		int record;
		LBdistribution* initial();
		
		void report();
		int num_sites;
		
		int get_position(int num);
		int get_number(int pos);
		int get_position(int x,int y,int z);
		int get_number(int x,int y,int z);
		
		void get_pos_coord(int pos,int& x,int& y,int& z);
		void get_num_coord(int num,int& x,int& y,int& z);
		
		LBfloat T;
		LBfloat lambda;
		
	private:
		LBfloat* rho_ini;
		int* m;
		void read_grid();
		int* vnum;
		int* vpos;
		LBifstream mediumfile;
		void enumerate();
		LBfloat viscosity();
		int mirror_x;
		int mirror_y;
		int mirror_z;
		int mirror;
};

#endif

