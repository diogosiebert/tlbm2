#include <sstream>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <ctime>
#include <omp.h>
#include <vector>

#include "vtk.h"
#include "lbstream.h"
#include "test/shear.h"
#include "test/wave.h"
#include "test/stationary.h"
#include "test/coexistence.h"
#include "test/bubble.h"

using namespace std;

void collision(LBdistribution* f,LBdata& data);
void vtkout(LBdistribution* f,LBdata& data,int step);

int main(int argc, char* argv[])
{
	time_t seconds = time(NULL);
	time_t temp = time(NULL);
	time_t collision_time = 0;
	
	if (argc == 1) error("A name of the a file that contains the medium description must be specified");

	LBdata data(argv[1]);

	int step = 0;
	
	LBdistribution* f = data.initial();
	
	record_neighbors(data);

	vtkout(f,data,0);
	
	data.report();
	
	do {
		step++;

		if (step%10 == 0)
		{
			cout << "\t Calculating step number: ";
			cout.width(30);
			cout << setiosflags( ios_base::right ) << step << "/" << data.steps << "\r";
		}
		
		collision(f,data);
		stream(f);
		
		if (step%data.record==0) {
				vtkout(f,data,step);	
		}
		
	} while (step < data.steps);

	cout << endl << "\t Simulation exited succefully." << endl; 
	cout << "\t Duration time: " << time(NULL)-seconds << " s" << endl << endl;
	
	return 0;
}

void vtkout(LBdistribution* f,LBdata& data,int step)
{
	LBmacro macro;
	LBfloat zero = 0.;
	string rho_name  = "RHO_.vtk";
	string vel_name = "VEL_.vtk";
	stringstream stepstream;
	stepstream << step;

	rho_name.insert(4,stepstream.str());
	vel_name.insert(4,stepstream.str());

	if (rho_name.size() < 17)
	{
		rho_name.insert(4,17-rho_name.size(),'0');
		vel_name.insert(4,17-vel_name.size(),'0');
	}

	int num;

	structured_points sp(data.Dx,data.Dy,data.Dz);

	vtkfile vtk_rho(rho_name.c_str(),&sp,vtkfile::SCALARS,"density",vtkfile::FLOAT,1);
	vtkfile vtk_vel(vel_name.c_str(),&sp,vtkfile::VECTORS,"velocity_field",vtkfile::FLOAT,1);

	int bpos = 0;
	int epos = data.Dim;

	LBfloat *r = new LBfloat[data.num_sites];

	#pragma omp parallel for
	for (int n=0;n<data.num_sites;n++)
	{
		r[n] = density(f[n]);
	}
	
	LBfloat lambda = data.lambda;
	LBfloat T = data.T;

	for (int pos=bpos;pos<epos;pos++)
	{
		num = data.get_number(pos);
		if (num!=-1) 
		{
			LBfloat gx = 0, gy =0, gz = 0;
			LBfloat glx = 0, gly = 0 , glz = 0;
	
			for (int i=0;i<b;i++)
			{
				int n_fw,x,y,z;
				data.get_pos_coord(pos,x,y,z);
				n_fw = data.get_number(x+cx[i],y+cy[i],z+cz[i]);
				LBfloat dgf = w[i] * (6. * a2-cc[i]*a4)/2. * ( (eos_ideal(r[n_fw],T) - data.pc*eos_waals(r[n_fw]/data.rc,T) )/r[num] );
				gx +=  dgf * cx[i]; gy +=  dgf * cy[i]; gz +=  dgf * cz[i]; 	 
			
				LBfloat dglr = w[i] * (a6*cc[i]-4*a4)  * r[n_fw];
				glx +=  dglr * cx[i]; 	gly +=  dglr * cy[i]; 	glz +=  dglr * cz[i]; 		
			}

			gx += lambda * glx;
			gy += lambda * gly;
			gz += lambda * glz; 
	
			macro.calculate(f[num]);
			macro.u(0) += 0.5*gx;
			macro.u(1) += 0.5*gy;
			macro.u(2) += 0.5*gz;
		}
		else 
		{
			macro.set(-1,0,0,0);
		}
		vtk_rho << static_cast<float>(macro.rho);
		vtk_vel << static_cast<float>(macro.u(0) );
		vtk_vel << static_cast<float>(macro.u(1) );
		vtk_vel << static_cast<float>(macro.u(2) );
	}
	vtk_rho.close();
	vtk_vel.close();
	delete r;
}

void collision(LBdistribution* f,LBdata& data)
{
	LBfloat *r = new LBfloat[data.num_sites];
	LBfloat freq = 1.0/(data.tau+0.5);
	LBfloat freq2 = a2*data.tau/(data.tau+0.5);
	
	int ni = 0;
	int nf = data.num_sites;
	
	#pragma omp parallel for
	for (int n=0;n<data.num_sites;n++)
	{
		r[n] = density(f[n]);
	}
	
	LBfloat lambda = data.lambda;
	LBfloat T = data.T;

	LBmacro macro;
	LBdistribution feq;		
		
	#pragma omp parallel for private(macro,feq)
	for (int n=ni ; n<nf ; n++)
	{
		
		LBfloat gx = 0, gy =0, gz = 0;
		int n_fw,x,y,z;
		int pos = data.get_position(n); 
		data.get_pos_coord(pos,x,y,z);	
	
		for (int i=0;i<b;i++)
		{
			n_fw = data.get_number(x+cx[i],y+cy[i],z+cz[i]);
			LBfloat dP = eos_ideal(r[n_fw],T) - data.pc*eos_waals(r[n_fw]/data.rc,T);
			LBfloat dgf = w[i] * ( 0.5*(6. * a2-cc[i]*a4) * ( dP /r[n] ) + (a6*cc[i]-4*a4)  * lambda*r[n_fw]);
			gx +=  dgf * cx[i];  gy +=  dgf * cy[i]; gz +=  dgf * cz[i]; 	 
		}
		
		macro.calculate(f[n]);
		macro.u(0) += 0.5*gx; macro.u(1) += 0.5*gy; macro.u(2) += 0.5*gz;
		macro.equilibrium(feq);
		
		for(int i=0;i<b;i++)
		{
			LBfloat F = ( gx * ( cx[i]-macro.u(0) ) + gy * ( cy[i]-macro.u(1) ) + gz * ( cz[i]-macro.u(2) ) );
			f[n][i] = (1.-freq)*f[n][i] + feq[i]*(freq + freq2*F);
		}
	}

	delete r;

}
