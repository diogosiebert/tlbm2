#include "vtk.h"

generic_dataset::generic_dataset()
{
	DatasetType = STRUCTURED_POINTS;
	set_dimension(1,1,1);
}

void generic_dataset::set_type(int type)
{
	DatasetType = type;
}

generic_dataset::generic_dataset(int x,int y,int z)
{
	set_dimension(x,y,z);
}

void generic_dataset::set_dimension(int x,int y,int z)
{
	Dimension_x = x;
	Dimension_y = y;
	Dimension_z = z;
}

int generic_dataset::get_type()
{
	return DatasetType;
}

structured_points::structured_points()
{
	set_type(STRUCTURED_POINTS);
	set_origin(0,0,0);
	set_spacing(1,1,1);
}

void structured_points::set_spacing(int x,int y,int z)
{
	Spacing_x = x;
	Spacing_y = y;
	Spacing_z = z;
}

void structured_points::set_origin(int x,int y,int z)
{
	Origin_x = x;
	Origin_y = y;
	Origin_z = z;
}

structured_points::structured_points(int x,int y,int z)  : generic_dataset(x,y,z)
{
	set_type(STRUCTURED_POINTS);
	set_origin(0,0,0);
	set_spacing(1,1,1);
}



void vtkfile::close()
{
	outfile.close();
}

void vtkfile::operator<<(float floatdata)
{
	char one_byte;
	if (header==0) write_header();
	pointdata++;
	if (binary == 0)
	{
		outfile << floatdata;
		if ( pointdata % (multiplier * pdataset->Dimension_x) == 0) outfile << endl;
		else outfile << " ";
	}
	else 
	{
		char* swap = (char*) &floatdata;
		one_byte = swap[0]; swap[0] = swap[3]; swap[3] = one_byte;
		one_byte = swap[1]; swap[1] = swap[2]; swap[2] = one_byte;
		outfile.write(swap,sizeof(float));
	}
}

void vtkfile::operator<<(double floatdata)
{
	char one_byte;
	if (header==0) write_header();
	pointdata++;
	if (binary == 0)
	{
		outfile << floatdata;
		if ( pointdata % (multiplier * pdataset->Dimension_x) == 0) outfile << endl;
		else outfile << " ";
	}
	else 
	{
		char* swap = (char*) &floatdata;
		one_byte = swap[0]; swap[0] = swap[7]; swap[7] = one_byte;
		one_byte = swap[1]; swap[1] = swap[6]; swap[6] = one_byte;
		one_byte = swap[2]; swap[2] = swap[5]; swap[5] = one_byte;
		one_byte = swap[3]; swap[3] = swap[4]; swap[4] = one_byte;
		outfile.write(swap,sizeof(double));
	}
}

vtkfile::vtkfile(const char* filename,generic_dataset* p1)
{
	data = SCALARS;
	variable = FLOAT;
	numcomp = 1;
	outfile.open(filename,ios::binary);
	pdataset = p1;
	set_ascii();
	header = 0;
	comment = "No Comments";
	name_of_data = "generic_data";
	pointdata = 0;
}

void vtkfile::set_binary()
{
	binary = 1;
}

void vtkfile::set_ascii()
{
	binary = 0;
}

int vtkfile::write_header()
{
	string sdt[2] = {"SCALARS","VECTORS"};
	string sdv[2] = {"float","double"};
	header = 1;
	outfile << "# vtk DataFile Version 2.0" << endl;
	outfile << comment << endl;
	if (binary) outfile << "BINARY" << endl;
	else outfile << "ASCII" << endl;
		
	switch(pdataset -> get_type() )
	{
		case generic_dataset::STRUCTURED_POINTS:
			structured_points* p = (structured_points*) pdataset;
			outfile << "DATASET STRUCTURED_POINTS" << endl;	
			outfile << "DIMENSIONS " << p->Dimension_x << " " << p->Dimension_y << " " << p->Dimension_z << endl;
			outfile << "ORIGIN " << p->Origin_x << " " << p->Origin_y << " " << p->Origin_z << endl;
			outfile << "SPACING " << p->Spacing_x << " " << p->Spacing_y << " " << p->Spacing_z << endl;
			outfile << "POINT_DATA " << (p->Dimension_x)*(p->Dimension_y)*(p->Dimension_z) << endl;
			break;
	}

	outfile << sdt[data] << " " << name_of_data << " " << sdv[variable] << endl; // " " << numcomp << endl;

	switch(data)
	{
		case SCALARS:
			outfile << "LOOKUP_TABLE default" << endl;
			multiplier = 1;
			break;
		case VECTORS:
			multiplier = 3;
			break;
	}

	return 0;
}

void vtkfile::set_data(int datatype,string dataname,int variabletype,int numberofcomp)
{
	data = datatype;
	name_of_data = dataname;
	variable = variabletype;
	numcomp = numberofcomp;
}

vtkfile::vtkfile(const char* filename,generic_dataset* p1,int datatype,string dataname,int variabletype,int numberofcomp)
{
	data = SCALARS;
	variable = FLOAT;
	numcomp = 1;
	outfile.open(filename,ios::binary);
	pdataset = p1;
	set_ascii();
	header = 0;
	comment = "No Comments";
	pointdata = 0;
	set_data(datatype,dataname,variabletype,numberofcomp);
}
