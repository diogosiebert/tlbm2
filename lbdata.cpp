#include "lbdata.h"
#include "lbeos.h"

string str_dimension = "DIMENSIONS";
string str_steps = "NUMBER_OF_STEPS";
string str_record = "RECORDING_FREQUENCY";
string str_tau = "RELAXATION_TIME";
string str_force = "EXTERNAL_FORCE";
string str_medium = "MEDIUM";
string str_rc = "CRITIC_DENSITY";
string str_pc = "CRITIC_PRESSURE";
string str_mirrorx = "MIRROR_X";
string str_mirrory = "MIRROR_Y";
string str_mirrorz = "MIRROR_Z";
string str_lambda = "LAMBDA";
string str_T = "TEMPERATURE";
string str_initial_rho = "INITIAL_RHO";

enum {SOLID,FLUID};

LBdata::LBdata(char* filename)
{
	mediumfile.open(filename,ios::binary);

	if (!mediumfile.is_open())
	{
		cout << "File \"" << filename << "\" Not Found! " << endl;
		exit(3);
	}

	int D[3] = {1,1,1};
	LBfloat Force[3] = {0,0,0};
	
	if(!mediumfile.seek_string("DATA_IN")) error("Keyword \"DATA_IN\" not found in the file!");
	if (!mediumfile.read_field(str_dimension,LBifstream::INT,3,D)) 	error(str_dimension + " field not found!");
	if (!mediumfile.read_field(str_steps,LBifstream::INT,1,&steps)) error(str_steps + " field not found!");
	if (!mediumfile.read_field(str_record,LBifstream::INT,1,&record)) error(str_record + " field not found!");
	if (!mediumfile.read_field(str_tau,LBifstream::FLOAT,1,&tau)) error(str_tau + " field not found!");

	mediumfile.read_field(str_force,LBifstream::FLOAT,3,Force);
		
	if (!mediumfile.read_field(str_mirrorx,LBifstream::INT,1,&mirror_x)) mirror_x = 0;
	if (!mediumfile.read_field(str_mirrory,LBifstream::INT,1,&mirror_y)) mirror_y = 0;
	if (!mediumfile.read_field(str_mirrorz,LBifstream::INT,1,&mirror_z)) mirror_z = 0;

	if (!mediumfile.read_field(str_lambda,LBifstream::FLOAT,1,&lambda)) lambda = 0;
	if (!mediumfile.read_field(str_rc,LBifstream::FLOAT,1,&rc)) rc = 1;
	if (!mediumfile.read_field(str_pc,LBifstream::FLOAT,1,&pc)) pc = 1;
	if (!mediumfile.read_field(str_T,LBifstream::FLOAT,1,&T)) T = 1.2;

	mirror = 0;
	if ( (mirror_x) || (mirror_y) || (mirror_z) ) mirror = 1;

	F.set(Force[0],Force[1],Force[2]);
		
	Dx = D[0]; Dy = D[1]; 	Dz = D[2];
	
	Dim = Dx*Dy*Dz;
	
	rho_ini = new LBfloat[Dim];
	
	if (!mediumfile.read_field(str_initial_rho,LBifstream::FLOAT,Dim,rho_ini)) 
	{
		delete rho_ini;
		rho_ini = NULL;
	}
	else if (!mediumfile.compare_next("END_" + str_initial_rho))error("Too much data in " + str_initial_rho); 
	
	read_grid();	
	mediumfile.close();
}

void LBdata::read_grid()
{
	string text;
	m = new int[Dim];
	mediumfile.read_field(str_medium,LBifstream::INT,Dim,m);
	if (!mediumfile.compare_next("END_" + str_medium))error("Too much data in " + str_medium);
	enumerate();	
	delete[] m;
}

LBdistribution* LBdata::initial()
{
	LBdistribution* f = new LBdistribution[num_sites];
	LBfloat* r = new LBfloat[num_sites];
	LBfloat rho= 1.0 ,ux = 0.0 ,uy = 0.0 ,uz = 0.0;
	LBmacro macro;

	int pos;
	macro.set(rho,ux,uy,uz);	
	for (int num=0;num<num_sites;num++)
	{	
		if (rho_ini != NULL) macro.set(rho_ini[num],ux,uy,uz);
		macro.equilibrium(f[num]);
		r[num] = rho_ini[num];
	}	
	
	for (int n=0;n<num_sites;n++)
	{
		LBfloat gx = 0, gy =0, gz = 0;
		LBfloat glx = 0, gly = 0 , glz = 0;
	
		for (int i=0;i<b;i++)
		{
			int n_fw,x,y,z;
			int pos = get_position(n); 
			get_pos_coord(pos,x,y,z);
			n_fw = get_number(x+cx[i],y+cy[i],z+cz[i]);
			LBfloat dgf = w[i] * (6. * a2-cc[i]*a4)/2. * ( (eos_ideal(r[n_fw],T) - pc*eos_waals(r[n_fw]/rc,T) )/r[n] );
			gx +=  dgf * cx[i];
			gy +=  dgf * cy[i];
			gz +=  dgf * cz[i]; 	 
			
			LBfloat dglr = w[i] * (a6*cc[i]-4*a4)  * r[n_fw];
			glx +=  dglr * cx[i];
			gly +=  dglr * cy[i];
			glz +=  dglr * cz[i]; 		
		}

		gx += lambda * glx;
		gy += lambda * gly;
		gz += lambda * glz;
		
		macro.calculate(f[n]);
		
		for (int i=0;i<b;i++)
		{
			LBfloat F = f[n][i]*a2*( gx * ( cx[i]-macro.u(0) ) + gy * ( cy[i]-macro.u(1) ) + gz * ( cz[i]-macro.u(2) ) );
			f[n][i] = f[n][i] - 6./12. * F;		}
 
	}
	
	delete r;
	return f;
}

void LBdata::enumerate()
{
	vnum = new int[Dim];
	num_sites = 0;
	
	int num = 0;
	
	for (int pos=0;pos<(Dx*Dy*Dz);pos++)
	{
		if (m[pos] == FLUID)
		{
				vnum[pos] = num++;
				num_sites++;
		}		
		else vnum[pos] = -1;
	}
			
	vpos = new int[num_sites];
	
	for (int pos=0;pos<Dx*Dy*Dz;pos++)
	{	
				num = vnum[pos];
				if (num != -1) vpos[num] = pos;			
	}		
}

LBfloat LBdata::viscosity()
{
		return tau/a2;
}

void LBdata::report()
{
	cout << endl << "\t NonI LBM - v0.1" << endl;
	cout << "\t LMPT - Laboratório de Meios Porosos e propriedades Termofísicas" << endl;
	cout << "\t Non-Ideal Fluid Lattice Boltzmann." << endl << endl;
	cout << "\t Lattice: " << lattice_name << endl << endl;
	cout << "\t ----------- Macroscopic Properties of the fluids (lattice units) ------------ " << endl;
	cout << "\t \t Viscosity: " << viscosity() << endl;
	cout << "\t ----------- Model Parameters ------------ " << endl;
	cout << "\t \t Relaxation Time: " << tau << endl;
	cout << "\t \t Temperature: " << T	 << endl;
	cout << "\t \t Interation Parameter: " << lambda << endl;
	cout << "\t \t Critic Density: " << rc << endl;
	cout << "\t \t Critic Pressure: " << pc << endl << endl;
}

int LBdata::get_position(int num)
{
//	return vpos[num];
	return num;
}

int LBdata::get_number(int pos)
{
//	return vnum[pos];
	return pos;
}

int LBdata::get_number(int x,int y,int z)
{
	return get_number( get_position(x,y,z) );
}

int LBdata::get_position(int x,int y,int z)
{
	x = (x+Dx)%Dx;
	y = (y+Dy)%Dy;
	z = (z+Dz)%Dz;
	return z*Dx*Dy+y*Dx+x;
}

void LBdata::get_pos_coord(int pos,int& x,int& y,int& z)
{
	z = pos / (Dx*Dy);
	y = (pos % (Dx*Dy)) / Dx;
	x = (pos % (Dx*Dy)) % Dx;
}

void LBdata::get_num_coord(int num,int& x,int& y,int& z)
{
	get_pos_coord( get_position(num) , x , y, z);
}

