#ifndef LBMATH_H
#define LBMATH_H

#include <cmath>

#define num_elements 9
#define LBfloat double

class LBvector;

class LBtensor
{
	public:
		LBtensor();
		LBfloat get_element(int i,int j);
		void set_element( int i, int j, LBfloat value );
		void operator = (LBtensor tensor );
		LBtensor operator + (LBtensor T_ );
		LBtensor  operator - (LBtensor T_ );
		LBtensor operator * (LBfloat k );
		LBvector operator * (LBvector x);
		~LBtensor();
		void identity();
		friend class LBvector;
private:
		LBfloat T[9];
};

class LBvector
{
	public:
		LBvector();
		LBvector(LBfloat x,LBfloat y, LBfloat z);
		void set(LBfloat x, LBfloat y, LBfloat z);
		void set_element(int i,LBfloat value);
		LBvector operator + ( LBvector X );
		LBvector operator - ( LBvector X );
		LBvector operator * ( LBfloat k );
		LBvector operator / ( LBfloat k );
		LBfloat& operator() (int i);
		LBvector operator * ( LBtensor T );
		LBvector& operator /= (LBfloat k);
		LBvector& operator += (LBvector v) ;
		LBfloat operator * ( LBvector &x );
		LBtensor operator && ( LBvector x );
		LBfloat norm();
		~LBvector();
		LBvector& operator = (LBfloat);
		friend class LBtensor;
		friend LBvector operator*(LBfloat, LBvector&);
	private:
		LBfloat v[3];
		
		
}; 

LBvector operator*(LBfloat, LBvector&);


#endif
