#include "shear.h"

LBShearWaveDecay::LBShearWaveDecay(LBdistribution *h,LBdata& dt,LBfloat U0,LBfloat DU)
{
		dataout.open("shear.dat");
		data = &dt;
		f = h;
		u0 = U0;
		du = DU;
		initial_condition();
}

void LBShearWaveDecay::initial_condition()
{
	LBmacro mp;
	
	int Dx = data->Dx;
	int Dy = data->Dy;
	int Dz = data->Dz;
	
	LBfloat u = 0;
	
	LBfloat k = 2*3.14159268/static_cast<float>(Dx);
	
	for (int z=0;z<Dz;z++)  {
	for (int x=0;x<Dx;x++) 	{
	for (int y=0;y<Dy;y++)
	{
			int n = data->get_number(x,y,z);
			u = u0 + du*cos(k*(x+0.5));
			mp.set(1,0,u,0);
			mp.equilibrium(f[n]);
	} 	}   }
}

void LBShearWaveDecay::write(int step,int x,int y,int z)
{
		int num = data->get_number(x,y,z);
		LBmacro m;
		m.calculate(f[num]);
		LBfloat nu = data->tau/a2;
		LBfloat L = data->Dx;
		dataout.precision(10);
		dataout << nu*step/(L*L) << " " << (m.u(1)-u0)/(-du) << endl;
}
