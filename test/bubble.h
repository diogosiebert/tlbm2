#ifndef BUBBLE_H
#define BUBBLE_H

#include "../lbeos.h"
#include "../lbdata.h"
#include <fstream>
#include <vector>

class LBBubble
{
	public:
		LBBubble(LBdistribution *f,LBdata &dt,LBfloat rg,LBfloat rl,LBfloat r);
		void write();
	private:	
		LBdata* data;
		LBfloat R;
		LBfloat rhol;
		LBfloat rhog;
		LBdistribution* f;
		ofstream dataout;
		void initial_condition();
};

#endif
