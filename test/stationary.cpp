#include "stationary.h"

LBStationary::LBStationary(LBdistribution *h,LBdata &dt,LBfloat r0t,LBfloat drt)
{
		dataout.open("stationary.dat");
		speedout.open("speed.dat");
		data = &dt;
		f = h;
		r0 = r0t;
		dr = drt;
		initial_condition();
		rows = data->Dy;
		cols = 0;
}

void LBStationary::initial_condition()
{
	LBmacro mp;
	
	int Dx = data->Dx;
	int Dy = data->Dy;
	int Dz = data->Dz;
	
	LBfloat r = 0;
	LBfloat l = data-> Dy;
	
	for (int z=0;z<Dz;z++)  {
	for (int x=0;x<Dx;x++) 	{
	for (int y=0;y<Dy;y++)
	{
			int n = data->get_number(x,y,z);
			r = r0 + dr*sin(2*3.14159265358979*(y+0.5)/l);
			mp.set(r,0,0,0);
			mp.equilibrium(f[n]);
	} 	}   }
}

void LBStationary::write(int x, int z)
{
			cols++;
			for (int y=0;y< (data->Dy);y++)
			{
				int num = data->get_number(x,y,z);
				LBmacro m;
				m.calculate(f[num]);
				result.push_back( (m.rho -r0)/dr );
			}
}

void LBStationary::soundspeed(int step)
{
	int y = (data->Dy)/4;
		
	int num = data->get_number(0,y,0);
	LBmacro m;
	m.calculate(f[num]);
	
	speedout << (m.rho-r0)/dr << endl;
} 

LBStationary::~LBStationary()
{
	for (int i=0;i<rows;i++)
	{
		dataout << i+0.5;
		for (int j=0;j<cols;j++)
		{
			dataout  << " " << result[i+j*rows];
		}
		dataout << endl;
	}
	dataout.close();
}