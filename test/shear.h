#ifndef SHEAR_H
#define SHEAR_H

#include "../lbdata.h"
#include <fstream>

class LBShearWaveDecay
{
	public:
		LBShearWaveDecay(LBdistribution *h,LBdata &dt,LBfloat U0,LBfloat DU);
		void write(int step,int x,int y,int z);
	private:	
		LBdata* data;
		ofstream dataout;
		LBfloat u0;
		LBfloat du;
		LBdistribution* f;
		void initial_condition();
};

#endif