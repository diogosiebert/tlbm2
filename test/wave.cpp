#include "wave.h"

LBWavePropagation::LBWavePropagation(LBdistribution *h,LBdata &dt,LBfloat r0t,LBfloat drt,LBfloat lt)
{
		dataout.open("wave.dat");
		speedout.open("speed.dat");
		data = &dt;
		f = h;
		r0 = r0t;
		dr = drt;
		l = lt;
		initial_condition();
		rows = (data->Dx)/2;
		cols = 0;
}

void LBWavePropagation::initial_condition()
{
	LBmacro mp;
	
	int Dx = data->Dx;
	int Dy = data->Dy;
	int Dz = data->Dz;
	
	LBfloat r = 0;
	LBfloat x0 = 0.5*(data->Dx-1);
	
	for (int z=0;z<Dz;z++)  {
	for (int x=0;x<Dx;x++) 	{
	for (int y=0;y<Dy;y++)
	{
			int n = data->get_number(x,y,z);
//			r = r0 + dr*1/(1+(x-x0)*(x-x0)/(l*l));
			r = r0 + dr*exp(-(x-x0)*(x-x0)/(l*l));
			mp.set(r,0,0,0);
			mp.equilibrium(f[n]);
	} 	}   }
}

void LBWavePropagation::write(int y, int z)
{
			cols++;
			for (int x=0;x<(data->Dx)/2;x++)
			{
				int num = data->get_number(x,y,z);
				LBmacro m;
				m.calculate(f[num]);
				result.push_back( (m.rho -r0)/dr );
			}
}

void LBWavePropagation::soundspeed(int step)
{
	LBfloat x0 = 0.5*(data->Dx-1);
	LBfloat dx = 0;
	LBfloat den = 0;
		
	for (int x=0;x<=x0;x++)
	{
		int num = data->get_number(x,0,0);
		LBmacro m;
		m.calculate(f[num]);
		den += (m.rho-r0);
		dx += (x-x0)*(m.rho-r0);
	}
	
	LBfloat x_m;
	LBfloat y_m = 0;
	
	for (int x=0;x<=x0;x++)
	{
		int num = data->get_number(x,0,0);
		LBmacro m;
		m.calculate(f[num]);
		if (m.rho > y_m)
		{
			y_m = m.rho;
			x_m = x;
		}
	}
	
	speedout << step << " " <<  (x_m-x0)/static_cast<LBfloat>(step) << " " <<  dx/(den*step) << endl;
	 
}

LBWavePropagation::~LBWavePropagation()
{
	for (int i=0;i<rows;i++)
	{
		dataout << (i-0.5*(data->Dx-1));
		for (int j=0;j<cols;j++)
		{
			dataout  << " " << result[i+j*rows];
		}
		dataout << endl;
	}
	
	dataout.close();
}