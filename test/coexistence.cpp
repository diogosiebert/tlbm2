#include "coexistence.h"

LBCoexistence::LBCoexistence(LBdistribution *h,LBdata &dt,LBfloat rg,LBfloat rl)
{
		dataout.open("den.dat");
		data = &dt;
		f = h;
		rhol = rl;
		rhog = rg;
		initial_condition();
}

void LBCoexistence::initial_condition()
{
	LBmacro mp;
	
	int Dx = data->Dx;
	int Dy = data->Dy;
	int Dz = data->Dz;
	
	LBfloat L = data->Dx;
	
	LBfloat rm = 0.5*(rhog+rhol);
	LBfloat dr = 0.5*(rhol-rhog);
	LBfloat l = 0.1;
	LBfloat r;
	
	for (int z=0;z<Dz;z++)  {
	for (int x=0;x<Dx;x++) 	{
	for (int y=0;y<Dy;y++)
	{
			int n = data->get_number(x,y,z);
			r = rm + (-tanh(l*x)+tanh(l*(x-Dx/2))-tanh(l*(x-Dx)) ) *dr;
			mp.set(r,0,0,0);
			mp.equilibrium(f[n]);
	} 	}   }
}

void  LBCoexistence::write(int step)
{
			LBmacro mp;
			for (int n=0;n< data->num_sites; n++)
			{
				mp.calculate(f[n]);				
				dataout << mp.rho/data->rc << " ";
			}	
}
void LBCoexistence::write_profile()
{
	LBmacro mp;
	LBfloat rl,rg,rm,dr;
	ofstream fileout("profile.dat");
	int L = data->Dx;
	int n = data->get_number(0,0,0);
	mp.calculate(f[n]);
	rg = mp.rho;
	n = data->get_number(L/2,0,0);
	mp.calculate(f[n]);
	rl = mp.rho;
	rm = 0.5*(rl + rg);
	dr = 0.5*(rl - rg);
	LBfloat l0 = data->rc*sqrt(data->lambda/data->pc);
	LBfloat r2,r1 = 0;
	LBfloat ri = 0;
	for (int x=1;x<data->Dx/2;x++)
	{
		int n = data->get_number(x,0,0);
		mp.calculate(f[n]);
		r2 = mp.rho;
		if ((rm > r2) && (rm < r1)) ri = x - 1 + (rm - r1)/(r2 - r1);
		r1=r2;
	}
	for (int x=0;x<data->Dx/2;x++)
	{
		int n = data->get_number(x,0,0);
		mp.calculate(f[n]);
		fileout <<  x << " " << (x-ri)/l0 << " " << (mp.rho-rm)/dr << endl;
	}
	fileout.close();
}

LBCoexistence::~LBCoexistence()
{
	dataout.close();
}