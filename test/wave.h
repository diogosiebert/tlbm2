#ifndef WAVE_H
#define WAVE_H

#include "../lbdata.h"
#include <fstream>
#include <vector>

class LBWavePropagation
{
	public:
		LBWavePropagation(LBdistribution *h,LBdata &dt,LBfloat ro,LBfloat dr,LBfloat l);
		~LBWavePropagation();
		void write(int y, int z);
		void soundspeed(int step);
	private:	
		LBdata* data;
		ofstream dataout;
		ofstream speedout;
		int rf;
		LBfloat r0;
		LBfloat dr;
		LBfloat l;
		LBdistribution* f;
		int cols;
		int rows;
		void initial_condition();
		vector<LBfloat> result;
};

#endif
