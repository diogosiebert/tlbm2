#ifndef STATIONARY_H
#define STATIONARY_H

#include "../lbdata.h"
#include <fstream>
#include <vector>

class LBStationary
{
	public:
		LBStationary(LBdistribution *h,LBdata &dt,LBfloat ro,LBfloat dr);
		~LBStationary();
		void write(int y, int z);
		void soundspeed(int step);
	private:	
		LBdata* data;
		ofstream dataout;
		ofstream speedout;
//		int rf;
		LBfloat r0;
		LBfloat dr;
		LBdistribution* f;
		int cols;
		int rows;
		void initial_condition();
		vector<LBfloat> result;
};

#endif
