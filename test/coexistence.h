#ifndef COEXISTENCE_H
#define COEXISTENCE_H

#include "../lbeos.h"
#include "../lbdata.h"
#include <fstream>
#include <vector>

class LBCoexistence
{
	public:
		LBCoexistence(LBdistribution *h,LBdata &dt,LBfloat rg,LBfloat rl);
		~LBCoexistence();
		void write(int step);
		void write_profile();
	private:	
		LBdata* data;
		ofstream dataout;
		LBfloat rhol;
		LBfloat rhog;
		LBdistribution* f;
		void initial_condition();
};

#endif
