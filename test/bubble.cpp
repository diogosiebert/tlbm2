#include "bubble.h"

LBBubble::LBBubble(LBdistribution *h,LBdata &dt,LBfloat rg,LBfloat rl,LBfloat Radius)
{
		data = &dt;
		f = h;
		rhol = rl;
		rhog = rg;
		R = Radius;
		dataout.open("surface.dat");
		initial_condition();
}

void LBBubble::initial_condition()
{
	LBmacro mp;
	
	int Dx = data->Dx;
	int Dy = data->Dy;
	int Dz = data->Dz;
	
	LBfloat L = data->Dx;
	
	LBfloat rm = 0.5*(rhog+rhol);
	LBfloat dr = 0.5*(rhol-rhog);
	LBfloat l = 0.5;
		
	LBfloat xc = (Dx-1)*0.5;
	LBfloat yc = (Dy-1)*0.5;
	LBfloat zc = (Dz-1)*0.5;
	LBfloat r,eta;
	
	for (int z=0;z<Dz;z++)  
	{
		for (int x=0;x<Dx;x++) 	
		{
			for (int y=0;y<Dy;y++)
			{
				eta = sqrt(  (x-xc)*(x-xc) + (y-yc)*(y-yc) + (z-zc)*(z-zc) );
				int n = data->get_number(x,y,z);
				r = rm - dr*tanh(l*(eta-R));
				mp.set(r,0,0,0);
				mp.equilibrium(f[n]);
			} 	
		}   
	}
}

void LBBubble::write()
{
	LBmacro mp;
	
	int Dx = data->Dx;
	int Dy = data->Dy;
	int Dz = data->Dz;
	
	LBfloat P_int, P_ext;
	
	mp.calculate(f[data->get_number((Dx-1)/2,(Dy-1)/2,0)]);
	LBfloat rl = mp.rho;
	
	mp.calculate(f[data->get_number(0,0,0)]);
	LBfloat rv = mp.rho;
	
	P_int = eos_waals(rl/data->rc,data->T);
	P_ext = eos_waals(rv/data->rc,data->T);
	
	LBfloat rm = 0.5*(rl+rv);
	LBfloat rf, rb;
	LBfloat ri[2] = {0,0};
	
	rb = rv;
	int i = 0;
	int x = 1;
	while (x<Dx || i < 2)
	{
		mp.calculate(f[ data->get_number(x,(Dy-1)/2,0) ]);
		rf = mp.rho;
		if ( (rf - rm)*(rb - rm) < 0 ) 	ri[i++] = static_cast<float>(x) - 1.0 + (rm - rb)/(rf - rb);
		rb = rf;
		x++;
	}
	
	dataout << 0.5*(ri[1]-ri[0])/(data->rc * sqrt(data->lambda/data->pc)) << " " << (P_int - P_ext);
	
	ofstream profile("bubble_profile.dat");
	for (int x = 0; x < Dx ; x++ )
	{
		mp.calculate(f[data->get_number(x,(Dy-1)/2,0)]);
		profile << mp.rho << " ";
	}
	profile.close();
}