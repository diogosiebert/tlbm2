#include "lbmath.h"

LBtensor::LBtensor()
{
	for (int n=0;n<num_elements;n++) T[n] = 0;
}

void LBtensor::identity()
{
	for (int n=0;n<num_elements;n++) T[n] = 0; 
	T[0] = 1;
	T[3] = 1;
	T[6] = 1;
}

LBfloat LBtensor::get_element(int i, int j)
{
	return T[j*3+i]; 
}

void LBtensor::set_element(int i, int j,LBfloat value)
{
	T[j*3+i] = value;
}

void LBtensor::operator =(LBtensor tensor)
{
	for (int n=0;n<num_elements;n++)
	{
		T[n] = tensor.T[n];
	}
}

LBtensor LBtensor::operator +(LBtensor tensor)
{
	LBtensor temp;
	for (int n=0;n<num_elements;n++)
	{
			temp.T[n] = T[n] + tensor.T[n];		
	}
	return temp;
}

LBtensor LBtensor::operator -(LBtensor tensor)
{
	LBtensor temp;
	for (int n=0;n<num_elements;n++)
	{
		temp.T[n] = T[n] - tensor.T[n];
	}
	return temp;
}

LBtensor LBtensor::operator *(LBfloat scalar)
{
	LBtensor temp;
	for (int n=0;n<num_elements;n++)
	{
		temp.T[n] = T[n] * scalar;
	}
	return temp;
}

LBvector LBtensor::operator *(LBvector x)
{
	LBvector temp;
	temp.v[0] = T[0]*x.v[2] + T[3]*x.v[1] + T[6]*x.v[0];
	temp.v[1] = T[1]*x.v[2] + T[4]*x.v[1] + T[7]*x.v[0];
	temp.v[2] = T[2]*x.v[2] + T[5]*x.v[1] + T[8]*x.v[0];
	return temp;
}

LBtensor::~LBtensor()
{
}

LBvector::LBvector()
{
	set(0,0,0);
};

LBvector::LBvector(LBfloat x, LBfloat y, LBfloat z )
{
	set(x,y,z);
}

LBfloat& LBvector::operator () (int i)
{
	return v[i];
}

void LBvector::set(LBfloat x,LBfloat y,LBfloat z)
{
	v[0] = x;
	v[1] = y;
	v[2] = z;
}

void LBvector::set_element(int i,LBfloat value)
{
	v[i] = value;
}

LBvector LBvector::operator +(LBvector x)
{
	LBvector temp;
	for (int n=0;n<3;n++)
	{
			temp.v[n] = v[n] + x.v[n];
	}
	return temp;
};

LBvector& LBvector::operator +=(LBvector x)
{
	v[0] += x.v[0];
	v[1] += x.v[1];
	v[2] += x.v[2];
	return *this;
}

LBvector LBvector::operator -(LBvector x)
{
	LBvector temp;
	for (int n=0;n<3;n++)
	{
			temp.v[n] = v[n] - x.v[n];
	}
	return temp;
};

LBvector LBvector::operator *(LBfloat scalar)
{
	LBvector temp;
	temp.v[0] = v[0] * scalar;
	temp.v[1] = v[1] * scalar;
	temp.v[2] = v[2] * scalar;
	return temp;
};

LBvector LBvector::operator /(LBfloat scalar)
{
	LBfloat inverse = 1./scalar;
	return (*this) * inverse;
};

LBvector LBvector::operator *(LBtensor tensor)
{
	LBvector temp;
	temp.v[0] = tensor.T[0]*v[0] + tensor.T[3]*v[1] + tensor.T[6]*v[2];
	temp.v[1] = tensor.T[1]*v[0] + tensor.T[4]*v[1] + tensor.T[7]*v[2];
	temp.v[2] = tensor.T[2]*v[0] + tensor.T[5]*v[1] + tensor.T[8]*v[2];
	return temp;
}

LBtensor LBvector::operator &&(LBvector x )
{
	LBtensor temp;
	for ( int i = 0; i < 3; i++ )
	{
		for ( int j = 0; j < 3; j++ )
		{
			temp.T[j*3+i] = v[i] * x.v[j];
		}
	}
	return temp;
}

LBfloat LBvector::operator*(LBvector &x)
{
	return v[0]*x.v[0] + v[1]*x.v[1]+ v[2]*x.v[2];
}

LBfloat LBvector::norm()
{
	return sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
};

LBvector& LBvector::operator/=(LBfloat k)
{
		v[0] /= k;
		v[1] /= k;
		v[2] /= k;
		return *this;
}

LBvector::~LBvector()
{
};

LBvector operator*(LBfloat scalar, LBvector& x)
{
		LBvector result;
		result.v[0] = scalar * x.v[0];
		result.v[1] = scalar * x.v[1];
		result.v[2] = scalar * x.v[2];
		return result;
}

LBvector& LBvector::operator = (LBfloat value)
{
		v[0] = value;
		v[1] = value;
		v[2] = value;
		return *this;
}
