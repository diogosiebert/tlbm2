#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

class generic_dataset
{
	public:
		generic_dataset();
		generic_dataset(int x,int y,int z);
		void set_dimension(int x,int y,int z);
		int get_type();
		enum {STRUCTURED_POINTS,STRUCTURED_GRID,UNSTRUCTURED_GRID,POLYDATA,RECTILINEAR_GRID,FIELD};
		friend class vtkfile;
		void set_type(int);
	private:
		int DatasetType;
		int Dimension_x;
		int Dimension_y;	
		int Dimension_z;
};

class structured_points : public generic_dataset
{
	public:
		structured_points();
		structured_points(int x,int y,int z);
		void set_spacing(int x,int y,int z);
		void set_origin(int x,int y,int z);
		friend class vtkfile;
	private:
		int Origin_x;
		int Origin_y;
		int Origin_z;
		int Spacing_x;
		int Spacing_y;
		int Spacing_z;
};

class vtkfile
{
	public:
		vtkfile(const char* filename,generic_dataset* p1);
		vtkfile(const char* filename,generic_dataset* p1,int datatype,string dataname,int variabletype,int numcomp);
		void set_binary();
		void set_ascii();
		void set_data(int datatype,string dataname,int variabletype,int numcomp);
		void operator<<(float);
		void operator<<(double);
		void close();
		enum {SCALARS,VECTORS};
		enum {FLOAT,DOUBLE};
	private:
		int pointdata;
		int multiplier;
		int numcomp;
		int data;
		int variable;
		int write_header();
		ofstream outfile;
		string comment;
		bool binary;
		bool header;
		generic_dataset* pdataset;
		string name_of_data;
};
