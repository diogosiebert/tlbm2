#include "lbstream.h"

vector<int> stream_list;
int num_sites;

void stream(LBdistribution* f)
{
	int ni = 0;
	int ngb_num;
	int ngb_dir;
	LBfloat* F = (LBfloat* ) f;
	LBfloat temp;	
	
// Switch the distribution in the direction i with the direction -i in all the sites
	#pragma omp parallel for
	for (int n=ni ; n<num_sites ; n++)
	{
		for (int k=0;k<bin;k++) 
		{	
			floatswitch( f[n][ in[k]], f[n][ out[k] ]);			
		}
	} 
	
// Switch the distribution between sites
	#pragma omp parallel for private(ngb_num)
	for (int n=ni ; n<num_sites ; n++)
	{
		for (int k=0;k<bin;k++) 
		{
			ngb_num = stream_list[n*bin+k];
			floatswitch( f[n][ out[k]], f[ngb_num][ in[k] ]);			
		}
	}
}

inline void floatswitch(LBfloat &v1,LBfloat &v2)
{
	LBfloat v3 = v1;
	v1 = v2;
	v2 = v3;
}

void record_neighbors(LBdata& data)
{
	num_sites = data.num_sites;
	for (int n=0 ; n<num_sites ; n++)
	{
		for (int k=0;k<bin;k++) 
		{
			int i = in[k];
			int x,y,z;

			data.get_num_coord(n,x,y,z);
			int ngb_num = data.get_number(x+cx[i],y+cy[i],z+cz[i]);

			stream_list.push_back(ngb_num);
		}
	}
}