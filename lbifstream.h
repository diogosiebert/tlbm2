#ifndef IFSTREAM_H
#define IFSTREAM_H

#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace std;

#include "lbmath.h"

void error(string comment);

class LBifstream : public ifstream
{
	public:
		LBifstream();
		int read_int(string);
		float read_float(string);
		bool seek_string(string);
		bool seek_string(string tobefound,string beg,string end);
		int get_line();
		bool compare_next(string tobecompared);
 		int read_field(string,int ,int ,void* );
		enum {INT,FLOAT};
	private:
		string text;
};

#endif
